# dwm

My dwm config

## Integrated Patches:

| Patch          | Effect                                                    |
|----------------|-----------------------------------------------------------|
| alpha          | See-through top bar                                       |
| alwayscenter   | Spawn new floating windows in the center of the screen    |
| attachaside    | Spawn new windows in stack rather than as master          |
| autostart      | Enables an autostart script to be run on dwm start        |
| cfacts         | Manage the size of slave stack windows                    |
| dwmblocks      | Clickable status bar modules                              |
| pertag         | Each tag can have an independantly changed layout         |
| scratchpads    | Toggleable scratchpads                                    |
| stacker        | Manage the order of stack windows                         |
| statusallmons  | Show statusbar on all monitors                            |
| statuscmd      | Clickable status bar modules                              |
| systray        | Enable systray support                                    |
| swallow        | Swallow terminal windows when they spawn a gui app        |
| vanitygaps     | Pretty gaps                                               |
| windowrolerule | Easier to define window rules                             |
| xrdb           | Easily change and reload variables                        |
| zoomswap       | Swap current window with the previous master when zooming |

## Integrated Layouts

- Tile
- Monocle
- Bstack
- Deck
- Centered Master
- Centered Floating Master

## Integrated Tweaks:
- New terminals spawn st
- Alt+L locks screen with slock
- Super+Shift+F spawns Finance Tracker (GnuCash)
- Super+n spawns newsboat
- Super+v spawns volumemixer (pulsemixer)
- Super+p spawns dmenu_recent_alias wrapper (custom script of mine (not included))
- Super+b spawns browser (firefox-fakehome - custom script of mine (not included))
- Super+c spawns calculator (galculator)
- Super+Shift+v spawns vit for task tracking

### Keybinds:
I have MODKEY defined as the Super Key (aka Windows Key).
To enable all scratchpad functionality and avoid errors, the following programs are required:
1. `dmenu`
2. `slock`
3. `st`
4. `lf`
5. `when`
6. `newsboat`
7. `pulsemixer`
8. `gnucash`
9. `galculator`

#### Audio and Brightness Control
Audio mute and volume up/down will require the following program to be installed:
- `pactl`

#### Program Spawning & Commands

| Modifier       | Key    | Effect                                                                     |
|----------------|--------|----------------------------------------------------------------------------|
| MODKEY         | p      | Execute dmenu_run                                                          |
| MODKEY + Shift | Return | Spawn st                                                                   |
| ALT            | l      | Lock screen using i3lock                                                   |
| MODKEY         | \`     | Toggle scratchpad (spawn floating centered st)                             |
| MODKEY         | e      | Toggle file explorer - opens lf in home directory                          |
| MODKEY + Shift | w      | Open `when` in editor allowing the addition or deletion of upcoming events |


#### Window and View Management
| Modifier       | Key    | Effect                       |
|----------------|--------|------------------------------|
| MODKEY         | j      | Focus next window            |
| MODKEY         | k      | Focus previous window        |
| MODKEY + Shift | j      | Increment master count by +1 |
| MODKEY + Shift | k      | Increment master count by -1 |
| MODKEY         | l      | Increase master              |
| MODKEY         | h      | Shrink master                |
| MODKEY         | Return | Set focused window as Master |
| MODKEY         | Tab    | Go to previous tag           |
| MODKEY + Shift | c      | Kill active window           |


#### Layouts
| Modifier       | Key   | Layout                                                                                 |
|----------------|-------|----------------------------------------------------------------------------------------|
| MODKEY         | t     | Tile - Default View                                                                    |
| MODKEY + Shift | t     | Deck (Non-master windows are maximized in stack area like a deck of cards)             |
| MODKEY         | y     | Bottom Stack - Stack is arranged underneath master window                              |
| MODKEY + Shift | y     | Monocle                                                                                |
| MODKEY         | u     | Centered Master - Master window is centered with flanking stacks                       |
| MODKEY + Shift | u     | Centered Floating Master - Master window is centered and floating with flanking stacks |
| MODKEY         | Space | Set layout to previous                                                                 |
| MODKEY + Shift | Space | Set active window to floating                                                          |

#### Monitor and Tag Management
| Modifier       | Key | Effect                                 |
|----------------|-----|----------------------------------------|
| MODKEY         | ,   | Focus previous monitor                 |
| MODKEY         | .   | Focus next monitor                     |
| MODKEY + Shift | ,   | Move active window to previous monitor |
| MODKEY + Shift | .   | Move active window to next monitor     |
| MODKEY         | 1-9 | Switch to tag 1-9                      |
| MODKEY + Shift | 1-9 | Set active window to tag 1-9           |
| MODKEY         | 0   | View all tags                          |
| MODKEY + Shift | 0   | Set active window to all tags          |

### Additional Keybinds

Additional keybinds for gap adjustments, xrdb reloading, etc. can be found in [`config.def.h`](/config.def.h)
