/* Bar functionality */
#include "bar_indicators.c"
#include "bar_tagicons.c"
#include "bar.c"

#include "bar_alpha.c"
#include "bar_dwmblocks.c"
#include "bar_ltsymbol.c"
#include "bar_status.c"
#include "bar_statuscmd.c"
#include "bar_tags.c"
#include "bar_wintitle.c"
#include "bar_systray.c"

/* Other patches */
#include "attachx.c"
#include "autostart.c"
#include "cfacts.c"
#include "pertag.c"
#include "scratchpad.c"
#include "stacker.c"
#include "swallow.c"
#include "vanitygaps.c"
#include "zoomswap.c"
#include "xrdb.c"
/* Layouts */
#include "layout_facts.c"
#include "layout_bstack.c"
#include "layout_centeredmaster.c"
#include "layout_centeredfloatingmaster.c"
#include "layout_deck.c"
#include "layout_monocle.c"
#include "layout_tile.c"

